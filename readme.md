Projet pour le test technique PingFlow utilisant openstreetmap

Api search endpoint : https://nominatim.openstreetmap.org/search?<params>
Api detail endpoint : https://nominatim.openstreetmap.org/details?place_id=<value>

N'ayant pas encore assez de billes sur le montage de containers, les builds et lancement de serveurs sont à faire à la main

#Installation:
##front :
*installation des dépendances :*
npm install

*lancement du rendu :*
build du front : npm run-script build
ou lancer le serveur en mode dev : npm run-script watch

##back :
*installation des dépendances :*
npm install

*build du serveur*
npm run-script release

*lancement du serveur*
npm run-script serve

#Installation annexes :
##MongoDB
documentation : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
