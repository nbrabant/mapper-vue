import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
	constructor(private readonly usersService: UsersService) {}

	async validateUser(token: string): Promise<any> {
		return await this.usersService.findOneByToken(token);
	}

	async loginUser(username: string, password: string): Promise<any> {
		let user = await this.usersService.findOneByUsername(username);
		let passwordMatch = await this.usersService.validateCredentials(user, password);
		if (!passwordMatch) {
			throw new UnauthorizedException();
		}
		
		
	}
}
