import { Controller, Get, Post, UseGuards, Res, HttpStatus, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@Post('login')
	login(@Body() loginForm, @Res() res) {
		this.authService.loginUser(loginForm.username, loginForm.password)
			.then(response => {
				res.status(HttpStatus.OK).json(['My bearer token'])
			})
			.catch(err => {
				res.status(HttpStatus.UNAUTHORIZED).json({message: err.message})
			})
	}

	@Get('users')
	@UseGuards(AuthGuard('bearer'))
	findAll() {
		return [];
	}
}
