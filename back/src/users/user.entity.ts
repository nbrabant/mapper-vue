import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
	@PrimaryGeneratedColumn()
  	id: number;

	@Column({ length: 255 })
	username: string;

	@Column({ length: 255 })
	password: string;

	@Column({ length: 255 })
	token: string;
}