import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(User)
		private readonly userRepository: Repository<User>
	) {}

	async findOneByToken(token): Promise<any> {
		return await this.userRepository.find({ token: token })
	}

	async findOneByUsername(username: string): Promise<any> {
		return await this.userRepository.findOne({ username: username })
	}

	async validateCredentials(user: User, password: string): Promise<Boolean> {
		return false
	}
}
