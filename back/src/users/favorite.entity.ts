import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Favorite {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('int')
	user_id: number;

	@Column('int')
	place_id: number;

	@Column({ length: 255 })
	display_name: string;

	@Column({ length: 255 })
	icon: string;

	@Column()
	comment: string;
	
	@Column()
  	active: boolean;

	@Column('int')
	position: number;
}