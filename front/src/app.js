import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import VueRouter from 'vue-router';
import store from './store';

Vue.use(VueRouter)
Vue.use(Vuex)

new Vue({
  el: '#app',
  router: new VueRouter({
      routes: [
        {
          path: '*',
          redirect: { name: 'login' }
        },
        {
          name: 'login',
          path: '/login',
          component: require('./pages/Login.vue').default
        },
        {
          name: 'register',
          path: '/register',
          component: require('./pages/Register.vue').default
        }
      ]
  }),
  store: store,
  render: h => h(App)
})
