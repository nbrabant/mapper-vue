import Vue from 'vue'

// Truncate string to the given length (default : 50 characters)
Vue.filter('truncate', function (value, length) {
    if (length === undefined) {
        length = 50
    }
    if (typeof value !== 'string' || value.length <= length) {
        return value
    }
    return value.substring(0, length).trim() + '...'
})
