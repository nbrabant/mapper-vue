const axios = require("axios");

const actions = {
	login({dispatch, commit}, {username, password}) {
		// @TODO : use env variables
		axios({
			method: 'post',
			url: 'http://localhost:3000/auth/login',
			headers: {
				'Access-Control-Allow-Origin': '*',
			},
			params: {
				username: username,
				password: password
			}
		})
		.then(response => {
			console.log(response);
		})
		.catch(error => {
			console.log(error);
		})
	},

	register({dispatch, commit}, {username, password}) {
		axios({
			method: 'post',
			url: 'http://localhost:3000/auth/register',
			headers: {
				'Access-Control-Allow-Origin': '*',
			},
			params: {
				username: username,
				password: password
			}
		})
		.then(response => {
			console.log(response);
		})
		.catch(err => {
			console.log(err);
		})
	}
}

export default {
	namespaced: true,
	actions
}